# cuda-gcc-7.3.1

```
git clone -b fedora-29 https://github.com/negativo17/cuda-gcc

# rpmbuild
sudo dnf install fedora-packager
# cuda-gcc requirements
sudo dnf install flex gmp-devel isl-devel libmpc-devel mpfr-devel
rpmdev-setuptree
cp cuda-gcc/cuda-gcc.spec ~/rpmbuild/SPECS
cp cuda-gcc/gcc-6.4.0-libatomic.patch ~/rpmbuild/SOURCES
cd rpmbuild/SOURCES
svn export svn://gcc.gnu.org/svn/gcc/branches/redhat/gcc-7-branch@258210 gcc-7.3.1-20180303
tar jcf gcc-7.3.1-20180303.tar.bz2 gcc-7.3.1-20180303
cd rpmbuild/SPECS
rpmbuild -ba cuda-gcc.spec
```
